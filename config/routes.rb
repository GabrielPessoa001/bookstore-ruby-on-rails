Rails.application.routes.draw do

  resources :categories
  resources :books
  devise_for :users

  root 'home#index'

  get 'ver_mais' => 'home#ver_mais_book'
  get 'index' => 'home#index'
  get 'destaques' => 'home#book_destaques'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
