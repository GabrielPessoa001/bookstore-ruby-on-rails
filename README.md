<%= image_tag( "Ruby_On_Rails_Logo.svg.png", :alt => "Ruby on Rails logo" ) %>

<br>

<p><strong>Ruby on Rails</strong> é um framework livre que promete aumentar velocidade e facilidade no desenvolvimento de sites orientados a banco de dados, uma vez que é possível criar aplicações com base em estruturas pré-definidas</p>

<br>

<p>Configurações e comandos utilizados abaixo:</p>

<br>

<ul>
  <li>Versão Ruby: 2.5.5 e a versão Rails: 5.2.4.3;</li>
  <li>Rodar o bundle install após a instalação e configuração do Ruby e Rails em sua máquina;</li>
  <li>Database: bancos estabelecidos em config/database.yml assim como as configurações do ambiente;</li>
  <li>Rodar os comandos para configuração do banco de dados: rake db:create db:migrate db:seed;</li>
  <li>Rode a aplicação utilizando o comando rails s</li>
</ul>