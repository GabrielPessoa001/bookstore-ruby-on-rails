json.extract! book, :id, :title, :author, :description, :selfLink, :imageLink, :language, :publisher, :publishedDate, :pageCount, :destaque, :category_id, :created_at, :updated_at
json.url book_url(book, format: :json)
