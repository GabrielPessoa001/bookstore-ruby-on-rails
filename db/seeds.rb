# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts 'Iniciando a criação de categorias'

puts '============================================>'

Category.create(description: 'romance')
Category.create(description: 'fábula')
Category.create(description: 'conto')
Category.create(description: 'crônica')
Category.create(description: 'ficção científica')
Category.create(description: 'fantasia')
Category.create(description: 'fantasia cintífica')
Category.create(description: 'horror')
Category.create(description: 'suspense')
Category.create(description: 'biografia')

puts 'Categorias criadas com sucesso'

puts '<============================================'

puts 'Iniciando a criação de livros'

puts '============================================>'

10.times do

  b = Book.create(
    title: 'Crime e castigo',
    author: 'Fiodor Dostoievski',
    description: 'Crime e castigo é um daqueles romances universais que, concebidos no decorrer do romântico século XIX, abriram caminhos ao trágico realismo literário dos tempos modernos',
    category_id: 6,
  )

  b.image.attach(io: File.open('app/assets/images/crimeECastigo.jpg'), filename: 'crimeECastigo.jpg')

end

puts 'Livros criados com sucesso'

puts '<============================================'