class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :title
      t.string :author
      t.text :description
      t.string :selfLink
      t.string :imageLink
      t.string :language
      t.string :publisher
      t.date :publishedDate
      t.integer :pageCount
      t.string :destaque
      t.references :category

      t.timestamps
    end
  end
end
